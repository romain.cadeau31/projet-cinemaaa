# Cinémaaa (tchi tcha)

##Contexte du projet

Allez chercher les informations sur les films depuis OMDb API.

    Ne créez pas votre BDD avec du SQL, Laravel a des outils pour ça (migrations)
    Utilisez des seeds pour renseigner les données initiales. Attention aux relations dans Laravel (pour gérer les liens entre vos tables et classes).
    Laravel permet de gérer facilement l'authentification mais il faut y penser dès la création du projet!

    Lisez la documentation (et regardez les Laracasts si besoin)

# Équipe

1. Pierre
2. Romain
3. Sreeja

## Duration

08/02-21/02

Travail en groupes de 3.

Rendu avant le 21 février minuit.

## Livrables

-   Maquettes (desktop et mobile) - MPD - Diagrammes UML : use case, class diagram et sequence diagram - Asana - Version en ligne du site - Repo gitlab accessible (commits réguliers avec des libellés explicites, de l'ensemble du groupe) - PPP quotidien par le scrum master - Readme détaillé sur installation et utilisation Attention, l'équipe pédagogique se réserve le droit de ne pas corriger un projet qui ne contiendrait pas tous les livrables.

## git

https://gitlab.com/Pierre_Cazeneuve_Carbonne/projet-cinemaaa

## figma

https://www.figma.com/file/bbQiL5xI3FM0LnzsfjpKUv/Untitled?node-id=21%3A2
https://www.figma.com/file/bbQiL5xI3FM0LnzsfjpKUv/Untitled?node-id=9%3A2
https://www.figma.com/file/bbQiL5xI3FM0LnzsfjpKUv/Untitled?node-id=0%3A1
https://www.figma.com/file/bbQiL5xI3FM0LnzsfjpKUv/Untitled?node-id=20%3A5
https://www.figma.com/file/bbQiL5xI3FM0LnzsfjpKUv/Untitled?node-id=22%3A2
https://www.figma.com/file/bbQiL5xI3FM0LnzsfjpKUv/Untitled?node-id=30%3A0
https://www.figma.com/file/bbQiL5xI3FM0LnzsfjpKUv/Untitled?node-id=23%3A0
https://www.figma.com/file/bbQiL5xI3FM0LnzsfjpKUv/Untitled?node-id=30%3A145

## asana

https://app.asana.com/0/1199914599486758/board

# User Stories

## Internaute

    En tant qu'internaute non-connecté, je dois me connecter pour accéder aux fonctionnalités d'utilisateur ou d'administrateur (y compris la liste des films).

## Utilisateur

    En tant qu'utilisateur, j'accède à la page d'accueil du site pour consulter la liste des films disponibles.
    En tant qu'utilisateur, je peux trier la liste des films par titre, durée ou année de sortie.
    En tant qu'utilisateur, je peux rechercher un film dans la liste par mot-clé (présent dans le titre ou le résumé de celui-ci).
    En tant qu'utilisateur, je peux filtrer les films de la liste par disponibilité.
    En tant qu'utilisateur, depuis la liste, je peux ajouter un ou plusieurs films à mon panier.
    En tant qu'utilisateur, depuis la liste, je peux accéder à mon panier afin de consulter la liste des films que je veux emprunter.
    En tant qu'utilisateur, depuis mon panier, je peux retourner à la liste des films.
    En tant qu'utilisateur, depuis mon panier, je peux valider ce panier pour emprunter les films.
    En tant qu'utilisateur, lorsque je valide mon panier, je suis ramené à la liste des films.
    En tant qu'utilisateur, lorsque je valide mon panier, mon panier est vidé.
    En tant qu'utilisateur, lorsque je valide mon panier, les films empruntés sont affichés sur la liste des films mais indiqués comme empruntés.
    En tant qu'utilisateur, depuis la liste des films, je peux rendre un ou plusieurs films qui sont alors à nouveau disponibles.

## Administrateur

    En tant qu'administrateur, depuis la liste des films, je peux accéder à l'écran d'ajout des films
    En tant qu'administrateur, depuis l'écran d'ajout des films, je peux ajouter un film.
    En tant qu'administrateur, depuis la liste des films, je peux accéder à l'écran de modification de chaque film.
    En tant qu'administrateur, depuis l'écran de modification d'un film, je peux modifier tous les champs d'un film sauf le titre.
    En tant qu'administrateur, depuis la liste des films, je peux supprimer un film.
    En tant qu'administrateur, lorsque je supprime un film, un écran de confirmation me permet de confirmer ou non mon choix.
    En tant qu'administrateur, depuis l'écran d'ajout des films, je peux chercher des films via l'API OMDB puis les ajouter à la liste.
    En tant qu'administrateur, sur l'écran de consultation d'un film, je peux voir les utilisateurs qui ont emprunté un exemplaire de ce film (avec la date de rendu souhaité et une indication s'il y a retard).
    En tant qu'administrateur, sur l'écran de consultation d'un film, je peux voir la liste des utilisateurs qui ont déjà emprunté et rendu un exemplaire de ce film.

​

## Bonus

Implémenter un système de réservation en commençant par rédiger les user stories correspondantes.
Modalités pédagogiques
